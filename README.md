# connect-four

Created with Ruby as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/ruby-programming/lessons/testing-your-ruby-code). 

# Final Thoughts

Completing this project allowed me to practice using RSpec, TDD methodologies and Object Oriented Programming in Ruby.
