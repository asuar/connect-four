# frozen_string_literal: true

class Board
  attr_accessor :row_size, :column_size
  attr_reader :filled_positions

  def initialize(row_size, column_size)
    self.row_size = row_size
    self.column_size = column_size
    @filled_positions = []
    @column_size.times  do
      row = []
      @row_size.times do
        row << 0
      end
      @filled_positions << row
    end
  end

  public

  def place_piece(column_position, current_player)
    return false if column_position < 0

    return false if column_position >= @column_size

    place_piece_in_row(column_position, current_player)
  end

  def row_size=(row_size)
    @row_size = if row_size >= 1
                  row_size
                else
                  0
                    # puts "Error: Board needs a row size greater than 0"
                  end
  end

  def column_size=(column_size)
    @column_size = if column_size >= 1
                     column_size
                   else
                     0
                     # puts "Error: Board needs a column size greater than 0"
                   end
  end

  def board_filled?
    @filled_positions.each do |row|
      return false if row.include? 0
    end
    true
  end

  def get_board_winner(connect_amount)
    result = check_line(@filled_positions, connect_amount) # check rows
    result = check_line(@filled_positions.transpose, connect_amount) if result == 0 # check columns
    result = check_diagonal(connect_amount) if result == 0
    result
  end

  private

  def place_piece_in_row(column_position, current_player)
    if filled_positions[column_position].include? 0
      filled_positions[column_position].each_with_index do |_position, index|
        reverse_row = (index + 1) * -1
        if filled_positions[column_position][reverse_row] == 0
          filled_positions[column_position][reverse_row] = current_player
          return true
        end
      end
    else
      false
    end
  end

  def check_line(positions, match_amount)
    winning_player = 0
    count_column = Hash.new(0)
    positions.each do |row|
      row.each do |player|
        count_column[player] += 1 unless player == 0
      end
      count_result = count_column.select { |_player, count| count >= match_amount }
      winning_player = count_result.keys[0] unless count_result == {}
      count_column = Hash.new(0)
    end
    winning_player
  end

  def check_diagonal(connect_amount)
    check_distance = (connect_amount - 1)
    @filled_positions.each_with_index do |column, c_index|
      column.each_with_index do |_row, r_index|
        current_player = @filled_positions[c_index][r_index]
        next if current_player == 0

        # check down left
        if r_index - check_distance >= 0 && c_index + check_distance < @column_size
          if @filled_positions[c_index][r_index] == current_player &&
             @filled_positions[c_index + 1][r_index - 1] == current_player &&
             @filled_positions[c_index + 2][r_index - 2] == current_player &&
             @filled_positions[c_index + 3][r_index - 3] == current_player
            return current_player
            end
        end
        # check up left
        next unless r_index - check_distance >= 0 && c_index - check_distance >= 0
        if @filled_positions[c_index][r_index] == current_player &&
           @filled_positions[c_index - 1][r_index - 1] == current_player &&
           @filled_positions[c_index - 2][r_index - 2] == current_player &&
           @filled_positions[c_index - 3][r_index - 3] == current_player
          return current_player
          end
      end
    end
    0
  end
end
