class ConnectFour

    require_relative 'player.rb'
    require_relative 'board.rb'

    @@player1_piece_code = "\u26AB"
    @@player2_piece_code = "\u26AA"
    @@COLUMN_SIZE = 7
    @@ROW_SIZE = 6
    @@CONNECT_AMOUNT_NEEDED = 4

    def initialize(show_starting_screen)
      display_starting_screen if show_starting_screen
      initialize_players
      @board = Board.new(@@ROW_SIZE, @@COLUMN_SIZE)
      @turn_count = 0
    end

    public

    def game_over?
        return @board.board_filled? || @board.get_board_winner(@@CONNECT_AMOUNT_NEEDED) > 0
    end

    def play_turn
        @turn_count += 1
        puts "Turn: #{@turn_count}"
        display_board
        player_move = -1
        until place_piece(player_move)
            player_move  = get_player_move
        end
    end

    def display_winner
        display_board
        if game_over?
          winner = get_winner
          if winner == 0
            puts "Game was a draw! :("
          else
            puts "#{winner == 1? @player1.to_s : @player2.to_s} is the winner!"  
          end
          return true
        end
        false
    end

    private

    def display_starting_screen
        puts '********************************'
        puts '* Connect Four by Alain Suarez *'
        puts '********************************'
        puts "* One player is #{@@player1_piece_code.encode('utf-8')} 's while the *"
        puts "* other is #{@@player2_piece_code.encode('utf-8')} 's. The first     *"
        puts '* to get 4 of their marks in   *'
        puts '* a row (up, down, across, or  *'
        puts '* diagonally) is the winner.   *'
        puts '********************************'
    end

    def initialize_players
        @player1 = get_user_name(@@player1_piece_code)
        @player2 = get_user_name(@@player2_piece_code)
    
        if @player1 == @player2
          @player1 = Player.new("#{@player1} 1")
          @player2 = Player.new("#{@player2} 2")
        end
    end

    def get_user_name(player_piece)
      puts "What is the name of the player who will use #{player_piece} 's?"
      gets.chomp.capitalize
    end

    def display_board
        board_row = @@ROW_SIZE
        board_column = @@COLUMN_SIZE
        player_moves = @board.filled_positions

        puts "  1   2   3   4   5   6   7"
        @@ROW_SIZE.times{ |r_index| 
            print "+"
            @@COLUMN_SIZE.times{
                print '---+'
            }
            puts ""
            print "|"
            @@COLUMN_SIZE.times{ |c_index| 
                piece_display_string = " "
                unless player_moves[c_index][r_index].nil?
                    piece_display_string = @@player1_piece_code if player_moves[c_index][r_index] == 1
                    piece_display_string = @@player2_piece_code if player_moves[c_index][r_index] == 2
                end
                
                print " #{piece_display_string} |"
            }
            puts ""
        }
        print "+"
        @@COLUMN_SIZE.times{
            print '---+'
        }
        puts ""
    end

    def get_player_move
            current_player = @turn_count.odd? ? @player1 : @player2
            move_position = 0
            while move_position < 1 || move_position > @@COLUMN_SIZE
                puts "Enter a column number to place your piece #{current_player}!"
                move_position = gets.chomp.to_i
            end
            move_position
    end

    def place_piece(player_move)
        current_player = @turn_count.odd? ? @player1 : @player2
        return @board.place_piece((player_move-1), current_player == @player1 ? 1 : 2)
    end

    def get_winner
      @board.get_board_winner(@@CONNECT_AMOUNT_NEEDED)
    end

end
