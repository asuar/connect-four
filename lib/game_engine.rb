# frozen_string_literal: true

require_relative 'connect_four.rb'

play_again = true
while play_again
  game = ConnectFour.new(true)
  game.play_turn until game.game_over?
  game.display_winner

  user_input = ''
  while user_input != 'Y' && user_input != 'N'
    puts 'Want to play again? [Y/N]'
    user_input = gets.chomp.upcase
  end
  play_again = user_input == 'Y'
end

puts 'Thanks for playing!'
