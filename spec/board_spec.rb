# frozen_string_literal: true

require './lib/board.rb'

describe Board do
  subject(:board) { Board.new(row_size, column_size) }

  describe '.initialize' do
    context 'when given positive sizes' do
      let(:row_size) { 6 }
      let(:column_size) { 7 }

      it 'accepts row size' do
        expect(board.row_size).to eq(6)
      end

      it 'accepts size' do
        expect(board.column_size).to eq(7)
      end
    end

    context 'when given negative sizes' do
      let(:row_size) { -6 }
      let(:column_size) { -7 }

      it 'does not accept negative row sizes' do
        expect(board.row_size).to eq(0)
      end

      it 'does not accept negative column sizes' do
        expect(board.column_size).to eq(0)
      end
    end
  end

  describe '#place_piece' do
    let(:row_size) { 6 }
    let(:column_size) { 7 }
    # validate position
    # place the move

    context 'when given negative column' do
      it 'doesnt place the piece' do
        expect(board.place_piece(-1, 1)).to eq(false)
      end
    end

    context 'when given column position that is too large ' do
      it 'doesnt place the piece' do
        expect(board.place_piece(20, 1)).to eq(false)
      end
    end

    context 'when given an unfilled position' do
      it 'places the piece' do
        expect(board.place_piece(1, 1)).to eq(true)
      end
    end

    context 'when given a filled position' do
      it 'doesnt place the piece' do
        6.times { board.place_piece(1, 1) }
        expect(board.place_piece(1, 1)).to eq(false)
      end
    end
  end

  describe '#board_filled?' do
    let(:row_size) { 2 }
    let(:column_size) { 2 }

    context 'when board is filled' do
      it 'returns true' do
        4.times do |index|
          board.place_piece(index % 2, index % 2 + 1)
        end
        should be_board_filled
      end
    end

    context 'when board is not filled' do
      it 'returns false' do
        should_not be_board_filled
      end
    end
  end

  describe '#get_board_winner' do
    let(:row_size) { 4 }
    let(:column_size) { 4 }

    context 'when there is no winner' do
      it 'returns 0' do
        expect(board.get_board_winner(4)).to eq(0)
      end
    end

    context 'when there is a winner' do
      context 'from a completed row' do
        it 'returns winner' do
          4.times do |index|
            board.place_piece(index % 1, 1)
          end
          expect(board.get_board_winner(4)).to eq(1)
        end
      end

      context 'from a completed column' do
        it 'returns winner' do
          4.times do |index|
            board.place_piece(index % 4, 2)
          end
          expect(board.get_board_winner(4)).to eq(2)
        end
      end

      context 'from a completed up-left diagonal' do
        it 'returns winner' do
          board.place_piece(0, 2)
          3.times { board.place_piece(0, 1) }
          3.times { board.place_piece(1, 1) }
          2.times { board.place_piece(2, 1) }
          board.place_piece(3, 1)
          expect(board.get_board_winner(4)).to eq(1)
        end
      end

      context 'from a completed down-left diagonal' do
        it 'returns winner' do
          board.place_piece(0, 1)
          2.times { board.place_piece(1, 1) }
          3.times { board.place_piece(2, 1) }
          board.place_piece(3, 2)
          3.times { board.place_piece(3, 1) }
          expect(board.get_board_winner(4)).to eq(1)
        end
      end
    end
  end
end
