# frozen_string_literal: true

require './lib/connect_four.rb'
require './lib/board.rb'

describe ConnectFour do
  subject(:game) { ConnectFour.new(show_starting_screen) }
  let(:show_starting_screen) { false }
  before {
    allow_any_instance_of(Object).to receive(:gets).and_return('Alain')
    allow($stdout).to receive(:write)
  }

  describe '#play_turn' do
    context 'when playing a turn' do
        it 'displays turn count' do
            allow(game).to receive(:get_player_move).and_return(1)
            allow(game).to receive(:display_board)
            expect { game.play_turn }.to output(/Turn: .+\n/).to_stdout
        end

        it 'displays the board' do
            allow(game).to receive(:get_player_move).and_return(1)
            expect { game.play_turn }.to output(/---+/).to_stdout
        end

        context 'when playing a piece' do
            it 'places piece in valid position' do
            allow(game).to receive(:display_board)
            expect(game).to receive(:get_player_move).and_return(1).once
            game.play_turn
            end

            it 'doesnt place piece in invalid position' do
                allow(game).to receive(:display_board)
                expect(game).to receive(:get_player_move).and_return(-1,1).twice
                game.play_turn
            end
        end
    end 
  end

  describe '#display_winner' do
    context 'when there is no winner' do
        it 'returns false' do
          expect(game.display_winner).to eq(false)
        end
      end

    context 'when there is winner' do
        
        it 'returns true' do
          allow(game).to receive(:game_over?).and_return(true)
          allow(game).to receive(:get_winner).and_return(1)
          expect(game.display_winner).to eq(true)
        end
    end

    context 'when there is a draw' do
        it 'returns true' do
            allow(game).to receive(:game_over?).and_return(true)
            allow(game).to receive(:get_winner).and_return(0)
          expect(game.display_winner).to eq(true)
        end
    end
  end



end
# expect {game_engine.setup_game}.to output(/.+/).to_stdout
# expect(subject).to receive(:play_game)
